import React from 'react';
import ReactDOM from 'react-dom';

import ons from 'onsenui';

import {
  ListItem,
  BottomToolbar,
  Button,
  Radio,
  Icon,
  Input,
  Page,
  List,
  ListHeader,
  Tab,
  Toolbar,
  BackButton,
  Segment,
  Modal,
  Tabbar,
  Fab,
  Navigator
} from 'react-onsenui';

import GameContainer from './components/GameContainer';

class PlayerPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      players: []
    };
  };

  componentDidMount = () => {
    const save_string = localStorage.getItem('game_save');
    if (save_string) {
      const save = JSON.parse(save_string);
      if (save["turn_count"] > 0) {
        const that = this;
        ons.notification.confirm("Re-Charger ancienne partie?",
          {
            title: '', buttonLabels: ['Annuler', 'Confirmer'], callback: function (choice) {
              if (choice === 1) {
                console.log("Loading previous game")
                that.props.navigator.pushPage({ comp: GameContainer, props: { players: save.players, save: save } });

              }
            }
          });
      }
    }
  }

  renderToolbar = () => {
    return (
      <Toolbar>
        <div className='center'>Qui joue?</div>
      </Toolbar>
    );
  }

  startGame = () => {
    console.log("Starting");
    this.props.navigator.pushPage({ comp: GameContainer, props: { players: this.state.players } });
  };

  renderBottomToolbar = () => {
    if (this.state.players.length > 0) {
      return (<BottomToolbar  >
        <Button className='center' modifier='large outline' onClick={this.startGame}>Commencer</Button>
      </BottomToolbar>);
    }
    return null;
  }

  handleClick = () => {
    var that = this;
    ons.notification.prompt({
      message: 'Nom?',
      callback: function (playername) {
        if (playername.length > 0) {
          that.setState(function (prevState, props) {
            return {
              players: prevState.players.concat([playername])
            };
          });
        }
      }
    });
  };

  renderFixed = () => {
    return (
      <Fab
        onClick={this.handleClick}
        position='bottom right'>
        <Icon icon='fa-plus' />
      </Fab>
    );
  };

  renderPlayeur = (name, index) => {
    return (
      <ListItem key={index}>
        <div className='center'>
          {name}
        </div>
        <div className='right' onClick={() => this.setState({ players: this.state.players.filter((item, i) => i != index) })}>
          <Icon icon='fa-minus' />
        </div>
      </ListItem>
    );
  };

  render() {
    let content = null;
    let bottom = null;
    if (this.state.players.length > 0) {
      content = <List
        dataSource={this.state.players}
        renderRow={this.renderPlayeur}
        renderHeader={() => <ListHeader>Joueurs</ListHeader>}
      />;
    } else {
      content = <section style={{ margin: '16px' }}>
        <p>
          Veuillez ajouter au minimum un joueur
        </p>
      </section>;
    }
    return (
      <Page key={"Players"}
        renderToolbar={this.renderToolbar}
        renderFixed={this.renderFixed}
        renderBottomToolbar={this.renderBottomToolbar}
      >
        {content}
      </Page>
    );
  }
};

class YApp extends React.Component {
  constructor(props) {
    super(props);
  }
  renderPage(route, navigator) {
    route.props = route.props || {};
    route.props.navigator = navigator;
    return React.createElement(route.comp, route.props);
  }

  render() {
    return (
      <Navigator
        initialRoute={{ comp: PlayerPage }}
        renderPage={this.renderPage}
      />
    );
  }
};

ReactDOM.render(<YApp />, document.getElementById('app'));
