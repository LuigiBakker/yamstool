import React from 'react';
import ons from 'onsenui';

import {
  ListItem,
  BottomToolbar,
  Button,
  Radio,
  Icon,
  Input,
  Page,
  List,
  ListHeader,
  Tab,
  Toolbar,
  BackButton,
  Segment,
  Modal,
  Tabbar,
  Fab,
  Navigator
} from 'react-onsenui';

class GamePage extends React.Component {
  constructor(props) {
    super(props);

    var combinaisons = [
      {
        'name': 'As',
        'description': 'Somme des dés de valeur 1',
        'fn': function (draw) {
          return draw.filter((c) => c === 1).length;
        },
        'points': 0,
        'selected': false,
        'done': false
      },
      {
        'name': 'Deux',
        'description': 'Somme des dés de valeur 2',
        'fn': function (draw) {
          return draw.filter((c) => c === 2).length * 2;
        },
        'points': 0,
        'selected': false,
        'done': false
      },
      {
        'name': 'Trois',
        'description': 'Somme des dés de valeur 3',
        'fn': function (draw) {
          return draw.filter((c) => c === 3).length * 3;
        },
        'points': 0,
        'selected': false,
        'done': false
      },
      {
        'name': 'Quatre',
        'description': 'Somme des dés de valeur 4',
        'fn': function (draw) {
          return draw.filter((c) => c === 4).length * 4;
        },
        'points': 0,
        'selected': false,
        'done': false
      },
      {
        'name': 'Cinq',
        'description': 'Somme des dés de valeur 5',
        'fn': function (draw) {
          return draw.filter((c) => c === 5).length * 5;
        },
        'points': 0,
        'selected': false,
        'done': false
      },
      {
        'name': 'Six',
        'description': 'Somme des dés de valeur 6',
        'fn': function (draw) {
          return draw.filter((c) => c === 6).length * 6;
        },
        'points': 0,
        'selected': false,
        'done': false
      },
      {
        'name': 'Brelan',
        'description': '3 dés identiques. Somme des 3 dés identiques',
        'fn': function (draw) {
          if (draw[0] === draw[2])
            return draw[0] * 3;
          else if (draw[1] == draw[3])
            return draw[1] * 3;
          else if (draw[2] == draw[4])
            return draw[2] * 3;
          return 0;
        },
        'points': 0,
        'selected': false,
        'done': false
      },
      {
        'name': 'Carré',
        'description': '4 dés identiques. Somme des 4 dés identiques',
        'fn': function (draw) {
          if (draw[0] === draw[3])
            return draw[0] * 4;
          else if (draw[1] == draw[4])
            return draw[1] * 4;
          return 0;
        },
        'points': 0,
        'selected': false,
        'done': false
      },
      {
        'name': 'Full',
        'description': '3 dés identiques + 2 dés identiques. Somme des dés',
        'fn': function (draw) {
          return (draw[0] === draw[2] && draw[3] === draw[4]) || (draw[0] === draw[1] && draw[2] === draw[4]) ? draw.reduce((p, c) => p + c) : 0;
        },
        'points': 0,
        'selected': false,
        'done': false
      },
      {
        'name': 'Petite Suite',
        'description': 'Suite de 4 dés. 30 points',
        'fn': function (draw) {
          draw = draw.filter(function (item, pos, ary) { return !pos || item != ary[pos - 1]; });
          return (draw[0] + 1 === draw[1] && draw[0] + 2 === draw[2] && draw[0] + 3 === draw[3]) ||
            (draw.length === 5 && draw[1] + 1 === draw[2] && draw[1] + 2 === draw[3] && draw[1] + 3 === draw[4]) ? 30 : 0;
        },
        'points': 0,
        'selected': false,
        'done': false
      },
      {
        'name': 'Grande Suite',
        'description': 'Suite de 5 dés. 40 points',
        'fn': function (draw) {
          return (draw[0] + 1 === draw[1] && draw[0] + 2 === draw[2] && draw[0] + 3 === draw[3] && draw[0] + 4 === draw[4]) ? 40 : 0;
        },
        'points': 0,
        'selected': false,
        'done': false
      },
      {
        'name': 'Yams',
        'description': '5 dés identiques. 50 points',
        'fn': function (draw) {
          return draw[0] === draw[4] ? 50 : 0;
        },
        'points': 0,
        'selected': false,
        'done': false
      },
      {
        'name': 'Chance',
        'description': 'Somme de tous les dés',
        'fn': function (draw) {
          return draw.reduce((p, c) => p + c);
        },
        'points': 0,
        'selected': false,
        'done': false
      }
    ];
    if (this.props.saved_state) {
      for (var i = 0; i < this.props.saved_state.length; ++i) {
        const save_comb = this.props.saved_state[i];
        var combinaisonIndex = combinaisons.findIndex((c) => c["name"] === save_comb["name"]);
        combinaisons[combinaisonIndex]["done"] = true;
        combinaisons[combinaisonIndex]["points"] = save_comb["points"];
      }
    }

    this.state = {
      draw: '',
      combinaisons: combinaisons
    };
  };
  preparseDraw = (callback) => {
    if (this.state.draw.replace(/[^0-9]]/gi).length !== 5)
      return 0;
    return callback(this.state.draw.split('').map((c) => parseInt(c)).sort());
  };

  isReadyToConfirm = () => {
    return this.state.draw.length === 5 && this.state.combinaisons.some((c) => c.selected);
  }
  getLowerScore = () => {
    return this.state.combinaisons.slice(0, 6).reduce(function (p, c) { return p + c['points']; }, 0);
  }
  getHigherScore = () => {
    return this.state.combinaisons.slice(6).reduce(function (p, c) { return p + c['points']; }, 0);
  }
  getScore = () => {
    var lowerScore = this.getLowerScore();
    var bonus = (lowerScore >= 63 ? 35 : 0)
    return lowerScore + this.getHigherScore() + bonus;
  }

  hasBonus = () => {
    return this.getLowerScore() > 62;
  }

  confimSelection = () => {
    var combinaisonIndex = this.state.combinaisons.findIndex((c) => c.selected);
    var combinaisons = this.removeSelections();
    combinaisons[combinaisonIndex].done = true;
    combinaisons[combinaisonIndex].points = this.preparseDraw(combinaisons[combinaisonIndex].fn);
    this.setState({ draw: '', combinaisons: combinaisons });
    const current_state = combinaisons.filter(function (c) { return c['done']; }).map(function (c) {
      return {
        'name': c["name"],
        'points': c["points"]
      };
    });
    this.props.nextPlayer(current_state);
  };

  handleLoad = (done) => {
    var that = this;
    done();
    ons.notification.confirm("Etes vous sure de quitter?",
      {
        title: '', buttonLabels: ['Annuler', 'Confirmer'], callback: function (choice) {
          if (choice === 1)
            that.props.navigator.popPage();
        }
      });
  };

  handleDrawChange = (e) => {
    var new_draw = e.target.value.replace(/[^1-6]/, '').slice(0, 5);
    this.setState({ draw: new_draw });
    if (new_draw.length !== 5)
      this.removeSelections();
  };

  removeSelections = () => {
    var neutralcombinaisons = this.state.combinaisons.map(function (c) { c.selected = false; return c; });
    this.setState({ combinaisons: neutralcombinaisons });
    return neutralcombinaisons;
  }

  reset = () => {
    this.setState({ draw: '' });
    this.removeSelections();
  }

  handleSelection(e) {
    if (!e.done && this.props.player === this.props.playerDrawing && this.state.draw.length === 5) {
      var combinaisons = this.removeSelections();
      var combinaisonindex = combinaisons.findIndex(function (c) { return e.name === c.name; });
      combinaisons[combinaisonindex]['selected'] = true;
      this.setState({ combinaisons: combinaisons });
    }
  };

  renderRow = (combinaison) => {
    let possiblepoint = null;
    if (!combinaison.done && this.props.player === this.props.playerDrawing && this.state.draw.length === 5) {
      possiblepoint = (<span className="list-item__subtitle">+{this.preparseDraw(combinaison.fn)}</span>);
    }
    return (
      <ListItem
        key={combinaison.name}
        modifier="longdivider material"
        tappable={!combinaison.done && this.props.player === this.props.playerDrawing}
        onClick={this.handleSelection.bind(this, combinaison)}
        style={{ border: combinaison.selected ? 'blue 2px solid' : '' }}>

        <label className='left'>
          <Radio
            inputId={"radio-" + combinaison.name}
            checked={combinaison.done}
            disabled={true}
          />
        </label>
        <label htmlFor={"radio-" + combinaison.name} className='center'>
          <span className="list-item__title">{combinaison.name}</span>
          <span className="list-item__subtitle">{combinaison.description}</span>
        </label>
        <div className='right'>
          <span className="list-item__title">{combinaison.points}</span>
          {possiblepoint}
        </div>
      </ListItem>
    );
  };




  renderBottomToolbar = () => {
    if (this.isReadyToConfirm()) {
      return (<BottomToolbar  >
        <Button className='center' modifier='large outline' onClick={this.confimSelection}>Confirmer selection</Button>
      </BottomToolbar  >);
    }
    else {
      return (<BottomToolbar  >
        <ListItem>
          <label className='center'>
            Total: {this.getScore()}
          </label>
          <label className='right'>
            Bonus: <Icon icon={this.hasBonus() ? 'fa-check' : 'fa-close'} /> ({this.getLowerScore()}/63)
          </label>
        </ListItem>
      </BottomToolbar  >);
    }
  }

  render() {
    let drawingSection = null;
    if (this.props.player === this.props.playerDrawing) {
      drawingSection = <section style={{ textAlign: 'center' }}>
        <p>
          <Input
            value={this.state.draw}
            onChange={this.handleDrawChange}
            modifier='material'
            float
            placeholder='Tirage'
            type="tel" />
          <Button onClick={this.reset} style={{ "marginLeft": '15px' }} modifier="outline">Clear</Button>
        </p>
        <p>
          <Button onClick={() => this.setState({ draw: (this.state.draw + '1').slice(0, 5) })} style={{ width: '25%', margin: '5px' }}>1</Button>
          <Button onClick={() => this.setState({ draw: (this.state.draw + '2').slice(0, 5) })} style={{ width: '25%', margin: '5px' }}>2</Button>
          <Button onClick={() => this.setState({ draw: (this.state.draw + '3').slice(0, 5) })} style={{ width: '25%', margin: '5px' }}>3</Button>
        </p>
        <p>
          <Button onClick={() => this.setState({ draw: (this.state.draw + '4').slice(0, 5) })} style={{ width: '25%', margin: '5px' }}>4</Button>
          <Button onClick={() => this.setState({ draw: (this.state.draw + '5').slice(0, 5) })} style={{ width: '25%', margin: '5px' }}>5</Button>
          <Button onClick={() => this.setState({ draw: (this.state.draw + '6').slice(0, 5) })} style={{ width: '25%', margin: '5px' }}>6</Button>
        </p>
      </section>
    }

    return (
      <Page renderBottomToolbar={this.renderBottomToolbar}>
        {drawingSection}
        <List
          dataSource={this.state.combinaisons}
          renderRow={this.renderRow}
          renderHeader={() => <ListHeader>Combinaisons</ListHeader>}
        />
      </Page>
    );
  };
}


module.exports = GamePage;
