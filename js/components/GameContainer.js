import React from 'react';
import ons from 'onsenui';

import {
  Button,
  Page,
  Tab,
  Toolbar,
  BackButton,
  Segment,
  Modal,
  Tabbar,
} from 'react-onsenui';

import GamePage from './GamePage';

class GameContainer extends React.Component {
  constructor(props) {
    super(props);
    var init_turn = 0;
    var player_index = 0;

    this.game_status = {
      turn_count: init_turn,
      players: this.props.players,
      combinaisons: []
    }

    if (this.props.save) {
      init_turn = this.props.save.turn_count;
      player_index = init_turn % this.props.players.length;
      this.game_status["turn_count"] = init_turn;
      this.game_status["combinaisons"] = this.props.save.combinaisons;
    }

    this.state = {
      segmentIndex: player_index,
      tabbarIndex: player_index,
      playerDrawing: this.props.players[player_index],
      turnsPlayed: init_turn,
      modalDone: false
    };
  };

  nextPlayer = (done_combinaison) => {
    this.game_status["combinaisons"][this.state.tabbarIndex] = done_combinaison;
    var nextIndex = (this.state.tabbarIndex + 1) % this.props.players.length;
    this.game_status["turn_count"] = this.state.turnsPlayed + 1;
    this.setState({ tabbarIndex: nextIndex, turnsPlayed: this.state.turnsPlayed + 1 });
    if (this.state.turnsPlayed + 1 < this.props.players.length * 13)
      this.setState({ playerDrawing: this.props.players[nextIndex] });
    else {
      this.setState({ playerDrawing: '' });
      this.game_status["turn_count"] = -1;
    }
    localStorage.setItem('game_save', JSON.stringify(this.game_status));

  }

  renderTabs = (activeIndex, tabbar) => {
    return this.props.players.map((c, i) => {
      return {
        content: <GamePage
          key={"player_page_" + i.toString()}
          player={c} active={activeIndex == i}
          nextPlayer={this.nextPlayer}
          playerDrawing={this.state.playerDrawing}
          saved_state={this.game_status["combinaisons"][i]}
          navigator={this.props.navigator} />,
        tab: <Tab key={"player_page_" + i.toString()} />
      }
    });
  }

  goBack = () => {
    const that = this;
    ons.notification.confirm("Etes vous sure de quitter?",
      {
        title: '', buttonLabels: ['Annuler', 'Confirmer'], callback: function (choice) {
          if (choice === 1) {
            const game_status = { "turn_count": -1 };
            localStorage.setItem('game_save', JSON.stringify(game_status));
            that.props.navigator.popPage();
          }
        }
      });
  }
  renderToolbar = () => {
    const buttonItems = this.props.players.map((player) =>
      <button key={"button_" + player.toString()}>{player}</button>
    );
    return (
      <Toolbar>
        <div className="center">
          <BackButton onClick={this.goBack}></BackButton>
          <Segment index={this.state.segmentIndex}
            onPostChange={() => this.setState({ segmentIndex: event.index })}
            tabbarId="tabbar"
            style={{ width: '80%', 'marginBottom': '3%', 'verticalAlign': 'middle' }}
          >
            {buttonItems}
          </Segment>
        </div>
      </Toolbar>
    );
  }

  renderModal = () => {
    return (<Modal isOpen={this.state.playerDrawing === '' && !this.state.modalDone}>
      <section style={{ margin: '16px' }}>
        <p>
          Game Over
        </p>
        <p>
          <Button onClick={() => this.setState({ modalDone: true })}>
            Ok
          </Button>
        </p>
      </section>
    </Modal>);
  }

  render() {
    return (
      <Page key={"GameTabs"}
        renderModal={this.renderModal}
        renderToolbar={this.renderToolbar}
      >
        <Tabbar
          id="tabbar"
          index={this.state.tabbarIndex}
          onPreChange={(event) => this.setState({ tabbarIndex: event.index })}
          renderTabs={this.renderTabs}
        />
      </Page>
    );
  }
};

module.exports = GameContainer;
